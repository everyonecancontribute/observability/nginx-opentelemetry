# Nginx Opentelemetry

This project provides a container image, and Kubernetes deployment for instrumenting Nginx with OpenTelementry. It also provides a curl demo deployment that triggers trace generation.

Inspired by the [Instrument Nginx with OpenTelemetry blog post](https://opentelemetry.io/blog/2022/instrument-nginx/). 

![Jaeger UI with ECC Traces](docs/images/jaeger_tracing_nginx_opentelemetry_instrumentation_k8s_deployed_ecc_observability.png)



## Requirements

- The OpenTelemetry webserver SDK 1.0.0 is built for Nginx 1.18.
- Kubernetes cluster deployed with[OpenTelemetry and Jaeger](#opentelemetry-and-jaeger-setup)
    - OpenTelemetry Collector listening on `otel-collector-collector.observability.svc.cluster.local:4317`

## Setup

- [Dockerfile](Dockerfile)
- [.gitlab-ci.yml](.gitlab-ci.yml) building the container image and pushing to the GitLab.com registry 
- [kubernetes/](kubernetes/)


## Deployment


```
kubectl apply -f kubernetes/manifests/nginx-otel-service.yml
```

Curl endless loop that queries the Nginx container to generate sample traces.

```
kubectl apply -f kubernetes/manifests/curl.yml
```

### Misc

Inspect logs.

```
kubectl logs -f service/nginx-otel-service
```

Pull a new image after building.

```
kubectl rollout restart deployment/nginx-otel-service
```

## OpenTelemetry and Jaeger Setup

> Complete setup documented in https://gitlab.com/everyonecancontribute/observability/k8s-o11y-chaos

Inspired by https://knative.dev/blog/articles/distributed-tracing/ with own adjustments.

* [cert-manager](https://cert-manager.io/docs/installation/) (required)
* [Jaeger Operator](https://github.com/jaegertracing/jaeger-operator#getting-started)
* [OpenTelemetry Operator](https://github.com/open-telemetry/opentelemetry-operator#getting-started)


```
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.8.1/cert-manager.yaml

kubectl create namespace observability

kubectl create -f https://github.com/jaegertracing/jaeger-operator/releases/download/v1.34.1/jaeger-operator.yaml -n observability 

kubectl apply -f https://github.com/open-telemetry/opentelemetry-operator/releases/latest/download/opentelemetry-operator.yaml


kubectl -n observability apply -f https://gitlab.com/everyonecancontribute/observability/k8s-o11y-chaos/-/raw/main/kubernetes/o11y/jaeger/simplest.yaml

```

Configure the collector, [review the configuration](https://gitlab.com/everyonecancontribute/observability/k8s-o11y-chaos/-/blob/main/kubernetes/o11y/opentelemetry/collector.yaml).

```
kubectl apply -f https://gitlab.com/everyonecancontribute/observability/k8s-o11y-chaos/-/raw/main/kubernetes/o11y/opentelemetry/collector.yaml
```
